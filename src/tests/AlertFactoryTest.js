describe('alert factory test', function () {
   beforeEach(module('ibexcore.alert'));

    var factory;

    beforeEach(inject(function (AlertFactory, AlertFactoryConfigProvider) {
        factory = AlertFactory;
        AlertFactoryConfigProvider.setTimeout(70000);
    }));

    it ('should add an alert with a default timeout of 7000 miliseconds', function () {
        factory.add('success', 'message');
        var alert = factory.get()[0];
        expect(alert.type).toBe('success');
        expect(alert.message).toBe('message');
        expect(alert.timeout).toBe(7000);
    });

    it ('should add an alert with a custom timeout', function () {
        factory.add('success', 'message', 7000);
        var alert = factory.get()[0];
        expect(alert.type).toBe('success');
        expect(alert.message).toBe('message');
        expect(alert.timeout).toBe(7000);
    });

    it ('should be able to change the default timeout', function () {
        factory.setDefaultTimeout(1000);
        expect(factory.getDefaultTimeout()).toBe(1000);
    });

    it ('should remove a message from the alerts array', function () {
        factory.add('success', 'message1');
        factory.add('warning', 'message2');
        expect(factory.get().length).toBe(2);
        factory.get()[0].close();
        expect(factory.get()[0].message).toBe('message2');
    });
});