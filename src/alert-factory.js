(function () {
    angular.module('ibexcore.alert', [])
        .run(function (AlertFactoryConfigProvider) {
            AlertFactoryConfigProvider.setTimeout(7000);
        })
        .factory('AlertFactory', function (AlertFactoryConfigProvider) {
            //console.log(AlertFactoryProvider.getTimeout())
            var alerts = [];
            var defaultTimeout = null;

            // constructor
            this.construct = function () {
                defaultTimeout = AlertFactoryConfigProvider.getTimeout();
            };

            this.construct();

            var factory = {
                add: function (type, message, timeout) {
                    timeout = typeof timeout != 'undefined' ? timeout : factory.getDefaultTimeout();
                    return factory.get().push({
                        type: type,
                        message: message,
                        close: function() {
                            return factory.close(this);
                        },
                        timeout: timeout
                    })
                },

                close: function (alert) {
                    return factory.closeByIndex(factory.get().indexOf(alert));
                },

                closeByIndex: function (index) {
                    return factory.get().splice(index, 1);
                },

                clear: function () {
                    alerts = [];
                },

                get: function () {
                    return alerts;
                },
                setDefaultTimeout: function (timeout) {
                    defaultTimeout = timeout;
                },
                getDefaultTimeout: function() {
                    return defaultTimeout;
                }
            };

            return factory;
        })
        .provider('AlertFactoryConfigProvider', function AlertFactoryConfigProvider(){
            var timeout = null;

            return {
                setTimeout: function (value) {
                    timeout = value;
                },
                getTimeout: function () {
                    return timeout;
                },
                $get: function () {
                    return this;
                }
            };
        })
        .directive('dismissAlertOnTimeout', function($timeout, $parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs){
                    // this seems very hacky
                    var timeout = parseFloat(attrs.dismissAlertOnTimeout);
                    //console.log(timeout, angular.isNumber(timeout), typeof timeout);
                    if (!isNaN(timeout)) {
                        $timeout(function () {
                            element.remove();
                        }, timeout)
                    }
                }
            };
        });
})();