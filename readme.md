# Angular bootstrap alert factory

## Getting started

After reading the documentation, take a look at example/index.html for examples.

You'll need to include the module in your module declaration 
`angular.module('yourApp', ['ibexcore.alert'])`

You'll need to create a controller to bind the alerts to the DOM

```
angular.module('yourApp', ['ibexcore.alert'])
    .controller('AlertController', function (alert) {
         var vm = this;
         vm.alerts = AlertFactory.get();
     });
```

Then, add this to your html page

```
<!-- we use ng-cloak to stop the flashing of elements before Angular parses them -->
<div data-ng-controller="AlertController as vm" class="container ng-cloak">
    <div ng-repeat="alert in vm.alerts"  class="alert" ng-class="'alert-' + alert.type" data-close="alert.close()" dismiss-alert-on-timeout="{{ alert.timeout }}">
        <button type="button" class="close ng-cloak" ng-click="alert.close()">
            <span aria-hidden="true">&times;</span>
        </button>
        <div ng-bind="alert.message"></div>
    </div>
</div>
```

To add an alert, just inject `AlertFactory` into your controller/service/factory:

`AlertFactory.add({type}, {message}, {timeout});`

**Type**: This is the alert type. You can specify whatever here, but the 
default Bootstrap alert types are info, warning, danger and success.

**Message**: Your message. By default, you can't put HTML in here. If you
wanted to include HTML, you'd have to include `angular-sanitize (ngSanitize)`
in your app and change `<div ng-bind="alert.message"></div>` to
`<div ng-bind-html="alert.message"></div>`

**Timeout**: This can either be blank, null or a number in milliseconds.
If it is left as blank, it'll use the default timeout, which we'll get to
in a second. If it is set to null, the alert will won't dismiss automatically.

## Changing the default timeout

By default, the automatic timeout of an alert is 7 seconds. To change this,
in your run block of your Angular JS app, inject `AlertFactoryConfigProvider`
and set setTimeout to whatever you want. Again, set this as a time in milliseconds
or set it to null for alerts never to dismiss automatically.

```
angular.module('yourApp', ['ibexcore.alert'])
    .run(function (AlertFactoryConfigProvider) {
        AlertFactoryConfigProvider.setTimeout(4000); // 4 seconds
        AlertFactoryConfigProvider.setTimeout(null); // Never dismiss automatically
    })
```
